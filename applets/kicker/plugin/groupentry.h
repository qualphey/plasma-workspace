/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include "abstractentry.h"

#include <QIcon>
#include <QPointer>
#include <QString>

class AppsModel;
class AbstractModel;

class GroupEntry : public AbstractGroupEntry
{
public:
    GroupEntry(AppsModel *parentModel, const QString &name, const QString &iconName, AbstractModel *childModel);

    QIcon icon() const override;
    QString name() const override;

    bool hasChildren() const override;
    AbstractModel *childModel() const override;

private:
    QString m_name;
    QString m_iconName;
    QPointer<AbstractModel> m_childModel;
};
