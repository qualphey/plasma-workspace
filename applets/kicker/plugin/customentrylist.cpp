/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "customentrylist.h"
#include "custommodel.h"

CustomEntryList::CustomEntryList(CustomModel *parent, QQmlListProperty<CustomEntry> entries)
    : QObject(parent)
{
}

QList<CustomEntry *> CustomEntryList::customEntries()
{
    return m_entries;
}

QQmlListProperty<CustomEntry> CustomEntryList::entries()
{
    return QQmlListProperty<CustomEntry>(this, &m_entries);
}
