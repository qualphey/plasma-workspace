/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "custommodel.h"
#include "actionlist.h"
#include "kastatsfavoritesmodel.h"
#include "recentcontactsmodel.h"
#include "recentusagemodel.h"
#include "systemmodel.h"

#include "groupentry.h"

#include <KLocalizedString>

#include <QCollator>

CustomModel::CustomModel(QObject *parent, CustomEntryList *customEntryList)
    : AppsModel(QString("///VIRTUAL///"), false, 24, false, false, true, parent)
    , m_favorites(new KAStatsFavoritesModel(this))
{
}

CustomModel::CustomModel(int subIndex, CustomModel *parent, KServiceGroup::Ptr group)
    : AppsModel(QString("///VIRTUAL///"), false, 24, false, false, true, parent)
    , m_subIndex(subIndex)
    , m_favorites(parent->kaStatsFavoritesModel())
    , m_parentModel(parent)
    , m_subGroup(true)
    , m_group(group)
{
    componentComplete();
}

CustomModel::~CustomModel()
{
}

CustomEntryList *CustomModel::customEntryList()
{
    return m_customEntryList;
}

void CustomModel::setCustomEntryList(CustomEntryList *customEntryList)
{
    m_customEntryList = customEntryList;
}

void CustomModel::refresh()
{
    if (!m_complete) {
        return;
    }

    beginResetModel();

    if (m_entryList.count()) {
        qDeleteAll(m_entryList);
        m_entryList.clear();
        emit cleared();
    }

    m_separatorCount = 0;

    if (!m_subGroup) {
        if (m_parsedCustomEntries.count()) {
            m_parsedCustomEntries.clear();
        }

        std::function<void(CustomEntry * customEntry, CustomEntry * rootEntry, KServiceGroup::Ptr groupEntry)> parseStoredGroup =
            [&](CustomEntry *customEntry, CustomEntry *rootEntry, KServiceGroup::Ptr groupEntry) {
                QList<KServiceGroup::Ptr> groupEntries = groupEntry->groupEntries();
                for (int i = 0; i < groupEntries.count(); i++) {
                    KServiceGroup::Ptr entry = groupEntries.at(i);
                    if (entry->childCount() > 0) {
                        QString eName = entry->caption();
                        QString eIcon = entry->icon();
                        QString eComment = entry->comment();

                        CustomEntry *ncgEntry = new CustomEntry(eName, eIcon, eComment, customEntry);

                        KServiceGroup::Ptr rentry = KServiceGroup::group(entry->relPath());

                        parseStoredGroup(ncgEntry, rootEntry, rentry);

                        customEntry->m_entries.append(ncgEntry);
                    }
                }

                QList<KService::Ptr> serviceEntries = groupEntry->serviceEntries();
                for (int i = 0; i < serviceEntries.count(); i++) {
                    KService::Ptr entry = serviceEntries.at(i);
                    CustomEntry *ncEntry = new CustomEntry(CustomEntry::StoredEntry, entry->storageId(), customEntry);

                    QString storageId = entry->storageId();
                    storageId.chop(8);
                    if (!rootEntry->exclude().contains(storageId) && !entry->noDisplay()) {
                        customEntry->m_entries.append(ncEntry);
                    }
                }
            };

        std::function<void(CustomEntry * customEntry, QList<CustomEntry *> * customEntries)> parseEntries = [&](CustomEntry *customEntry,
                                                                                                                QList<CustomEntry *> *customEntries) {
            if (customEntry->root()) {
                KServiceGroup::Ptr rentry = KServiceGroup::root();

                QList<KServiceGroup::Ptr> groupEntries = rentry->groupEntries();
                for (int i = 0; i < groupEntries.count(); i++) {
                    KServiceGroup::Ptr entry = groupEntries.at(i);
                    if (entry->childCount() > 0) {
                        QString eName = entry->caption();
                        QString eIcon = entry->icon();
                        QString eComment = entry->comment();

                        CustomEntry *ncgEntry = new CustomEntry(eName, eIcon, eComment, customEntry);
                        KServiceGroup::Ptr rentry = KServiceGroup::group(entry->relPath());
                        parseStoredGroup(ncgEntry, customEntry, rentry);

                        customEntries->append(ncgEntry);
                    }
                }

                QList<KService::Ptr> serviceEntries = rentry->serviceEntries();
                for (int i = 0; i < serviceEntries.count(); i++) {
                    KService::Ptr entry = serviceEntries.at(i);
                    qDebug() << entry->storageId() << i; // << customEntry->exclude();

                    QString storageId = entry->storageId();
                    storageId.chop(8);
                    if (!customEntry->exclude().contains(storageId) && !entry->noDisplay()) {
                        CustomEntry *ncEntry = new CustomEntry(CustomEntry::StoredEntry, entry->storageId(), customEntry);
                        customEntries->append(ncEntry);
                    }
                }

                customEntry->m_parsed = true;
            } else {
                CustomEntry *customEntryClone = new CustomEntry(customEntry, customEntry->parent());
                customEntries->append(customEntryClone);

                for (int i = 0; i < customEntry->m_entries.count(); i++) {
                    parseEntries(customEntry->m_entries.at(i), &customEntryClone->m_entries);
                }
            }
        };

        QList<CustomEntry *> customModelEntryList = customEntryList()->customEntries();
        for (int i = 0; i < customModelEntryList.count(); i++) {
            parseEntries(customModelEntryList.at(i), &m_parsedCustomEntries);
        }
    }

    KServiceGroup::Ptr group = m_subGroup ? m_group : KServiceGroup::virtualGroup("", "", "");
    for (int i = 0; i < m_parsedCustomEntries.count(); i++) {
        CustomEntry *customEntry = m_parsedCustomEntries.at(i);
        if (customEntry->separator()) {
            group->addEntry(KServiceGroup::separator());
            m_entryList << new SeparatorEntry(this);
            ++m_separatorCount;
        } else if (!customEntry->storageId().isNull() && !customEntry->storageId().isEmpty()) {
            const KService::Ptr &entry = KService::serviceByStorageId(customEntry->storageId());
            group->addEntry(entry);
            m_entryList << new AppEntry(this, entry, AppEntry::NameOnly);
        } else if (!customEntry->desktopName().isNull() && !customEntry->desktopName().isEmpty()) {
            const KService::Ptr &entry = KService::serviceByDesktopName(customEntry->desktopName());
            group->addEntry(entry);
            m_entryList << new AppEntry(this, entry, AppEntry::NameOnly);
        } else if (!customEntry->command().isNull() && !customEntry->command().isEmpty()) {
            const KService::Ptr &entry = KService::virtualService(customEntry->name(), customEntry->command(), customEntry->icon());
            group->addEntry(entry);
            m_entryList << new AppEntry(this, entry, AppEntry::NameOnly);
        } else if (customEntry->m_entries.count() > 0) {
            KServiceGroup::Ptr entry = KServiceGroup::virtualGroup(customEntry->name(), customEntry->icon(), customEntry->comment());
            group->addEntry(entry);
            AppGroupEntry *groupEntry = new AppGroupEntry(this, entry, false, m_pageSize, false, false, true, false, i);
            m_entryList << groupEntry;
        } else if (!customEntry->groupPath().isNull() && !customEntry->groupPath().isEmpty()) {
            KServiceGroup::Ptr entry = KServiceGroup::group(customEntry->groupPath());
            group->addEntry(entry);
            AppGroupEntry *groupEntry = new AppGroupEntry(this, entry, false, m_pageSize, false, false, true, false);
            m_entryList << groupEntry;
        } else if (!customEntry->systemAction().isNull() && !customEntry->systemAction().isEmpty()) {
            SystemEntry *entry = new SystemEntry(this, customEntry->systemAction());

            if (entry->isValid()) {
                m_entryList << entry;
            }

        } else if (customEntry->recentFiles()) {
            RecentUsageModel *recentModel = new RecentUsageModel(this, RecentUsageModel::OnlyDocs, RecentUsageModel::Recent, 6);
            QAbstractItemModel *srcModel = recentModel->sourceModel();
            for (int ri = 0; ri < srcModel->rowCount(); ri++) {
                QString fCommand = recentModel->resourceAt(ri);
                fCommand.prepend("xdg-run '");
                fCommand.append("'");
                QString fName = recentModel->data(recentModel->index(ri, 0)).toString();
                QString fIcon = recentModel->data(recentModel->index(ri, 0), Qt::DecorationRole).value<QIcon>().name();
                const KService::Ptr &entry = KService::virtualService(fName, fCommand, fIcon);
                group->addEntry(entry);
                m_entryList << new AppEntry(this, entry, AppEntry::NameOnly);
            }
        } else if (customEntry->favorites()) {
            QPointer<QAbstractItemModel> srcModelPTR = m_favorites->sourceModel();
            if (!srcModelPTR.isNull()) {
                QAbstractItemModel *srcModel = srcModelPTR.data();
                for (int ri = 0; ri < srcModel->rowCount(); ri++) {
                    QString fPath = m_favorites->data(m_favorites->index(ri, 0), Kicker::UrlRole).toString();
                    fPath.remove(0, 7);
                    const KService::Ptr &entry = KService::serviceByDesktopPath(fPath);
                    group->addEntry(entry);

                    m_entryList << new AppEntry(this, entry, AppEntry::NameOnly);
                }
            }
        }
    }
    endResetModel();

    emit refreshed();
}

QList<CustomEntry *> CustomModel::subEntry(int subIndex)
{
    QList<CustomEntry *> customModelEntryList = m_subGroup ? m_parentModel->subEntry(m_subIndex) : m_parsedCustomEntries;
    for (int i = 0; i < customModelEntryList.count(); i++) {
        CustomEntry *customEntry = customModelEntryList.at(i);
        if (customEntry->m_entries.count() > 0) {
            if (subIndex == i) {
                return customEntry->m_entries;
            }
        }
    }

    return QList<CustomEntry *>();
}

void CustomModel::classBegin()
{
}

void CustomModel::componentComplete()
{
    m_complete = true;
    QList<CustomEntry *> parsedCustomEntries = m_subGroup ? m_parentModel->subEntry(m_subIndex) : customEntryList()->customEntries();
    if (m_subGroup)
        m_parsedCustomEntries = parsedCustomEntries;

    for (int i = 0; i < parsedCustomEntries.count(); i++) {
        CustomEntry *customEntry = parsedCustomEntries.at(i);
        if (!customEntry->systemAction().isNull() && !customEntry->systemAction().isEmpty()) {
            SystemEntry *entry = new SystemEntry(this, customEntry->systemAction());
            QObject::connect(entry, &SystemEntry::isValidChanged, this, &AbstractModel::refresh, Qt::UniqueConnection);
        } else if (customEntry->recentFiles()) {
            RecentUsageModel *recentModel = new RecentUsageModel(this, RecentUsageModel::OnlyDocs, RecentUsageModel::Recent, 6);
            QPointer<QAbstractItemModel> srcModel = recentModel->sourceModel();
            QObject::connect(srcModel.data(), &QAbstractItemModel::dataChanged, this, &AbstractModel::refresh, Qt::UniqueConnection);
        } else if (customEntry->favorites()) {
            CustomModel *refresh_target = m_subGroup ? m_parentModel : this;
            QObject::connect(m_favorites, &PlaceholderModel::sourceModelChanged, refresh_target, &AbstractModel::refresh, Qt::UniqueConnection);
        }
    }

    refresh();
}

bool CustomModel::topLevel() const
{
    return m_topLevel;
}

void CustomModel::setTopLevel(bool topLevel)
{
    if (m_topLevel != topLevel) {
        m_topLevel = topLevel;
        emit topLevelChanged();
    }
}

AbstractModel *CustomModel::favoritesModel()
{
    return m_favorites;
}

KAStatsFavoritesModel *CustomModel::kaStatsFavoritesModel()
{
    return m_favorites;
}

AbstractModel *CustomModel::systemFavoritesModel()
{
    if (m_systemModel) {
        return m_systemModel->favoritesModel();
    }

    return nullptr;
}
