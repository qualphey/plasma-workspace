/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include "appsmodel.h"

#include "customentry.h"
#include "customentrylist.h"

#include <QObject>
#include <qqml.h>

class KAStatsFavoritesModel;
class RecentContactsModel;
class RecentUsageModel;
class SystemModel;

class CustomModel : public AppsModel
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(bool topLevel READ topLevel WRITE setTopLevel NOTIFY topLevelChanged)
    Q_PROPERTY(CustomEntryList *customEntryList READ customEntryList WRITE setCustomEntryList)
    QML_ELEMENT

public:
    explicit CustomModel(QObject *parent = nullptr, CustomEntryList *customEntryList = nullptr);
    explicit CustomModel(int subIndex, CustomModel *parent, KServiceGroup::Ptr group);
    ~CustomModel() override;

    CustomEntryList *customEntryList();
    void setCustomEntryList(CustomEntryList *customEntryList);

    void classBegin() override;
    void componentComplete() override;

    bool topLevel() const;
    void setTopLevel(bool topLevel = false);

    QList<CustomEntry *> subEntry(int subIndex);

    AbstractModel *favoritesModel() override;
    KAStatsFavoritesModel *kaStatsFavoritesModel();

    AbstractModel *systemFavoritesModel();

    QList<CustomEntry *> m_parsedCustomEntries = QList<CustomEntry *>();

Q_SIGNALS:
    void refreshed() const;
    void topLevelChanged() const;
    void systemFavoritesModelChanged() const;

protected Q_SLOTS:
    void refresh() override;

private:
    KAStatsFavoritesModel *m_favorites;
    SystemModel *m_systemModel;
    CustomEntryList *m_customEntryList;
    bool m_subGroup = false;
    KServiceGroup::Ptr m_group;
    int m_subIndex;
    CustomModel *m_parentModel;
    QObject *m_parent;
    bool m_topLevel = false;
};
