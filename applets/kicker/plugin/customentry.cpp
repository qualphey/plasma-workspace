/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "customentry.h"
#include "customentrylist.h"

CustomEntry::CustomEntry(CustomEntry *clone, CustomEntry *parent)
    : QObject(parent)
    , m_storageId(clone->storageId())
    , m_desktopName(clone->desktopName())
    , m_command(clone->command())
    , m_groupPath(clone->groupPath())
    , m_systemAction(clone->systemAction())
    , m_name(clone->name())
    , m_root(clone->root())
    , m_icon(clone->icon())
    , m_comment(clone->comment())
    , m_favorites(clone->favorites())
    , m_exclude(clone->exclude())
    , m_recentFiles(clone->recentFiles())
    , m_limit(clone->limit())
    , m_separator(clone->separator())
    , m_parent(parent)
{
}

CustomEntry::CustomEntry(CustomEntry *parent)
    : QObject(parent)
    , m_parent(parent)
{
}

CustomEntry::CustomEntry(Type type, QString storageId, CustomEntry *parent)
    : QObject(parent)
    , m_storageId(storageId)
    , m_parent(parent)
{
}

CustomEntry::CustomEntry(QString name, QString icon, QString comment, CustomEntry *parent)
    : QObject(parent)
    , m_name(name)
    , m_icon(icon)
    , m_comment(comment)
    , m_parent(parent)
{
}

QString CustomEntry::storageId() const
{
    return m_storageId;
}

void CustomEntry::setStorageId(QString storageId)
{
    if (m_storageId != storageId) {
        m_storageId = storageId;
    }
}

QString CustomEntry::desktopName() const
{
    return m_desktopName;
}

void CustomEntry::setDesktopName(QString desktopName)
{
    if (m_desktopName != desktopName) {
        m_desktopName = desktopName;
    }
}

QString CustomEntry::command() const
{
    return m_command;
}

void CustomEntry::setCommand(QString command)
{
    if (m_command != command) {
        m_command = command;
    }
}

QString CustomEntry::groupPath() const
{
    return m_groupPath;
}

void CustomEntry::setGroupPath(QString groupPath)
{
    if (m_groupPath != groupPath) {
        m_groupPath = groupPath;
    }
}

QString CustomEntry::systemAction() const
{
    return m_systemAction;
}

void CustomEntry::setSystemAction(QString systemAction)
{
    if (m_systemAction != systemAction) {
        m_systemAction = systemAction;
    }
}

bool CustomEntry::separator() const
{
    return m_separator;
}

void CustomEntry::setSeparator(bool separator)
{
    if (m_separator != separator) {
        m_separator = separator;
    }
}

QString CustomEntry::name() const
{
    return m_name;
}

void CustomEntry::setName(QString name)
{
    if (m_name != name) {
        m_name = name;
    }
}

QString CustomEntry::icon() const
{
    return m_icon;
}

void CustomEntry::setIcon(QString icon)
{
    if (m_icon != icon) {
        m_icon = icon;
    }
}
QString CustomEntry::comment() const
{
    return m_comment;
}

void CustomEntry::setComment(QString comment)
{
    if (m_comment != comment) {
        m_comment = comment;
    }
}

bool CustomEntry::favorites() const
{
    return m_favorites;
}

void CustomEntry::setFavorites(bool favorites)
{
    if (m_favorites != favorites) {
        m_favorites = favorites;
    }
}

bool CustomEntry::root() const
{
    return m_root;
}

void CustomEntry::setRoot(bool root)
{
    if (m_root != root) {
        m_root = root;
    }
}

QStringList CustomEntry::exclude() const
{
    return m_exclude;
}

void CustomEntry::setExclude(QStringList exclude)
{
    if (m_exclude != exclude) {
        m_exclude = exclude;
    }
}

bool CustomEntry::recentFiles() const
{
    return m_recentFiles;
}

void CustomEntry::setRecentFiles(bool recentFiles)
{
    if (m_recentFiles != recentFiles) {
        m_recentFiles = recentFiles;
    }
}

int CustomEntry::limit() const
{
    return m_limit;
}

void CustomEntry::setLimit(int limit)
{
    if (m_limit != limit) {
        m_limit = limit;
    }
}

QQmlListProperty<CustomEntry> CustomEntry::entries()
{
    return QQmlListProperty<CustomEntry>(this, &m_entries);
}

CustomEntry *CustomEntry::parent() const
{
    return m_parent;
}
