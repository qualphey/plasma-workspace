/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "groupentry.h"

#include "rootmodel.h"

GroupEntry::GroupEntry(AppsModel *parentModel, const QString &name, const QString &iconName, AbstractModel *childModel)
    : AbstractGroupEntry(parentModel)
    , m_name(name)
    , m_iconName(iconName)
    , m_childModel(childModel)
{
    QObject::connect(parentModel, &RootModel::cleared, childModel, &AbstractModel::deleteLater);

    QObject::connect(childModel, &AbstractModel::countChanged, [parentModel, this] {
        if (parentModel) {
            parentModel->entryChanged(this);
        }
    });
}

QString GroupEntry::name() const
{
    return m_name;
}

QIcon GroupEntry::icon() const
{
    return QIcon::fromTheme(m_iconName, QIcon::fromTheme(QStringLiteral("unknown")));
}

bool GroupEntry::hasChildren() const
{
    return m_childModel && m_childModel->count() > 0;
}

AbstractModel *GroupEntry::childModel() const
{
    return m_childModel;
}
