/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QObject>
#include <qqml.h>

#include "customentry.h"

class CustomModel;

class CustomEntryList : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<CustomEntry> entries READ entries)
    Q_CLASSINFO("DefaultProperty", "entries")
    QML_ELEMENT
public:
    CustomEntryList(CustomModel *parent = nullptr, QQmlListProperty<CustomEntry> entries = QQmlListProperty<CustomEntry>());
    QQmlListProperty<CustomEntry> entries();
    QList<CustomEntry *> customEntries();

private:
    QList<CustomEntry *> m_entries;
};
