/*
    SPDX-FileCopyrightText: 2021 Vitalius Kuchalskis <qualphey@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QObject>
#include <qqml.h>

#include "appentry.h"

class CustomEntry : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString storageId READ storageId WRITE setStorageId)
    Q_PROPERTY(QString desktopName READ desktopName WRITE setDesktopName)
    Q_PROPERTY(QString command READ command WRITE setCommand)
    Q_PROPERTY(QString groupPath READ groupPath WRITE setGroupPath)
    Q_PROPERTY(QString systemAction READ systemAction WRITE setSystemAction)
    Q_PROPERTY(bool separator READ separator WRITE setSeparator)
    Q_PROPERTY(bool favorites READ favorites WRITE setFavorites)
    Q_PROPERTY(bool root READ root WRITE setRoot)
    Q_PROPERTY(QStringList exclude READ exclude WRITE setExclude)
    Q_PROPERTY(bool recentFiles READ recentFiles WRITE setRecentFiles)
    Q_PROPERTY(int limit READ limit WRITE setLimit)

    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString icon READ icon WRITE setIcon)
    Q_PROPERTY(QString comment READ comment WRITE setComment)

    Q_PROPERTY(QQmlListProperty<CustomEntry> entries READ entries)
    Q_CLASSINFO("DefaultProperty", "entries")
    QML_ELEMENT
public:
    enum Type { StoredEntry };
    explicit CustomEntry(CustomEntry *clone, CustomEntry *parent = nullptr);
    explicit CustomEntry(CustomEntry *parent = nullptr);
    explicit CustomEntry(Type type, QString storageId, CustomEntry *parent = nullptr);
    explicit CustomEntry(QString name, QString icon, QString comment = QString(), CustomEntry *parent = nullptr);

    QString storageId() const;
    void setStorageId(QString storageId);

    QString desktopName() const;
    void setDesktopName(QString desktopName);

    QString command() const;
    void setCommand(QString command);

    QString groupPath() const;
    void setGroupPath(QString groupPath);

    QString systemAction() const;
    void setSystemAction(QString systemAction);

    bool separator() const;
    void setSeparator(bool separator = false);

    QString name() const;
    void setName(QString name);

    QString icon() const;
    void setIcon(QString icon);

    QString comment() const;
    void setComment(QString comment);

    bool favorites() const;
    void setFavorites(bool favorites);

    bool root() const;
    void setRoot(bool root);

    QStringList exclude() const;
    void setExclude(QStringList);

    bool recentFiles() const;
    void setRecentFiles(bool recentFiles);

    int limit() const;
    void setLimit(int limit);

    CustomEntry *parent() const;

    QQmlListProperty<CustomEntry> entries();
    QList<CustomEntry *> m_entries;

    bool m_parsed = false;

protected:
    CustomEntry *m_parent;
    QString m_storageId;
    QString m_desktopName;
    QString m_command;
    QString m_groupPath;
    QString m_systemAction;
    bool m_separator = false;
    bool m_favorites = false;
    bool m_root = false;
    QStringList m_exclude = QStringList();
    bool m_recentFiles = false;
    int m_limit = 6;

    QString m_name;
    QString m_icon;
    QString m_comment;

    AbstractEntry *m_abstractEntry;
};
